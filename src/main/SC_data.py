#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 08:25:40 2017

@author: dsdavis1
"""
import os.path
import numpy as np
import uuid, json
import pandas as pd

datafile = "data.csv"
print "Opening file: ",datafile, "in directory :", os.getcwd()
try:
    if os.path.isfile(datafile):
        data = pd.read_csv(datafile)
except IOError as e:
    print "Unable to open File" #file not found or cannot be opened
    raise
 
#
orig_len= len(data.index)  #save the original number of entries

try:
    pd.isnull(data).values.any() # check for missing data
except:
    print "Array has null values, needs cleaning"
    raise
 #
 #Note: visual inspection of UUID field shows values 8,9,a,b for the 
 #      4th groupd indicating these uuid are the the current version
 #      the 3rd field shows this s a random varient
 #      field 1 
 #
#
uid = uuid.UUID(data['uuid'][0])

print "Note UUID version is: ", uid.version
print "These are randomly generated"
node = data['uuid'][0]
data['node'] = data.uuid.str[4:8]
data['time'] = data.apply(lambda x: "", axis=1)
for x in range(0, orig_len):
    uid = uuid.UUID(data['uuid'][x])
    data.ix[x,'time'] = uid.time
# inline check if needed for debuging
#print data
#
print "Checking nodes", "for duplicate entries"
#the characters 5 - 8 should indicate the 
#data.drop_duplicates('uuid'[5:8], keep='first') #check the uuid column for duplicates
data.drop_duplicates('node', keep='first')
#
uniq_len = len(data.index)  #number of entries after removing duplicates
print orig_len - uniq_len, "entries were dupicate devices"

print "Total number of devices: ", orig_len
print "Total numner of unique devices:", uniq_len

data_region = data.query('lat >= 0.00 & lat <= 89.9 & long >= -179.99 & long <= 0.00')
print "Number of devices between -179.99 -> 0 long & 0 -> 89.99 lat is ",
len(data_region.index)

# find the oldest update time (clock 0 is 00:00:00.00, 15 October 1582) so we need 
# the smallest time & no need to decode
min_uuidTime =  min(data.time)
print "Minimum time(oldest)",min_uuidTime
index_min_old = np.argmin(data.time)
print "device is:", data.node[index_min_old]
# find the youngest update time (clock 0 is 00:00:00.00, 15 October 1582) so we need 
# the largest time & no need to decode
max_uuidTime =  max(data.time)
print "Maximum time(most recent)",max_uuidTime
index_min_young = np.argmax(data.time)
print "device is:", data.node[index_min_young]

# write results to a json format file
#first build a dataframe with the needed results
out_data={}
out_data['devices'] = []
out_data['devices'].append({
'total_devices': orig_len,
'uniq_devices' : uniq_len,
'Area_seleced_devices': len(data_region.index),
'Device with oldest update':  data.node[index_min_old],
'Device oldest update time':  min_uuidTime,
'Device with most recent update': data.node[index_min_young],
'Most recent update time': max_uuidTime                                   
 })
#write the dataframe to a json file
#use indent = 4 to make it more human readable
with open('results.json', 'w') as outfile:  
    json.dump(out_data, outfile, indent=4)
